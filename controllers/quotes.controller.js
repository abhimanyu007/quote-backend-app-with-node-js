const Quote = require('../modals/quotes.modal');
// Create and Save a new Quote
exports.create = (req, res) => {
    if(!req.body) {
        return res.status(400).send({
            message: 'Quote content should not be empty'
        });
    }
    // Create a Quote
    const quote = new Quote({
        id: req.body.id,
        person: req.body.person || "Untitled Note",
        text: req.body.text
    });
    // Save Note in the database
    quote.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

// Retrieve and return all quotes from the database.
exports.findAll = (req, res) => {
    Quote.find()
    .then(quotes => {
        console.log('quotes')
        res.send(quotes);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

// Find a single note with a noteId
exports.findOne = (req, res) => {
    Quote.findById(req.params.noteId)
    .then(quote => {
        if(!quote) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });            
        }
        res.send(quote);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.noteId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }

    // Find note and update it with the request body
    Quote.update({ "category": req.body.category ,"quotes._id": req.body['_id'],}, 
    {$set: { "quotes.$.isFavourite": req.body.isFavourite }}, {new: true})
    .then(results => {
        if(!results) {
            return res.status(404).send({
                message: "Note not found with id " + req.body['_id']
            });
        }
        res.send(results);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.body['_id']
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.body['_id']
        });
    });
};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Quote.findByIdAndRemove(req.params.noteId)
    .then(quote => {
        if(!quote) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.noteId
        });
    });
};


// Like and Dislike restAPI

exports.likeDisLikeQuote = (req,res) => {
    // Validate Request
    console.log('====================================');
    console.log(req);
    console.log('====================================');
    if(!req.body) {
        return res.status(400).send({
            message: "Quote content can not be empty"
        });
    }
    // Find note and update it with the request body
    Quote.update({ "category": req.body.category ,"quotes._id": req.body['_id']}, 
    {$set: { "quotes.$.like": req.body.like }}, {new: true})
    .then(results => {
        if(!results) {
            return res.status(404).send({
                message: "Note not found with id " + req.body['_id']
            });
        }
        Quote.find({ "category": req.body.category ,"quotes._id": req.body['_id']}).then(updatedQuote => {
            res.send({
              message: "Note deleted successfully! with id " + req.body['_id'],
              data: updatedQuote
            });
          }).catch(err => {
            res.status(500).send({
              message:
                err.message || "Some error occurred while retrieving notes."
            });
          });
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.body['_id']
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.body['_id']
        });
    });
}
