const Ingredients = require('../modals/ingredients.modal');
var ObjectId = require('mongodb').ObjectID;
// Retrieve and return all quotes from the database.
exports.findAll = (req, res) => {
    Ingredients.find()
    .then(ingredients => {
        res.send(ingredients);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

exports.update = (req,res) => {
    /* var myquery = { _id: "5e3e6a3300191857f5180b59" };
    var newvalues = {$set: {'ingredients.salad': 1} };
    Ingredients.updateMany(myquery, newvalues, function(err, res) {
        if (err) throw err;
        console.log(res);
    }) */
    if(!req.body) {
        return res.status(400).send({
            message: "Ingredients content can not be empty"
        });
    }
    const dataFromResponse = JSON.parse(req.body.action);
    const finalData = req.body.callType === 'remove'? removeIngredienthandler(dataFromResponse.data, dataFromResponse.ingredientType) : 
    addIngredientHandler(dataFromResponse.data, dataFromResponse.ingredientType);
    Ingredients.updateMany({
        "_id" : ObjectId(dataFromResponse['data']['_id'])
        },
        // update 
        {
            $inc : {[`ingredients.${dataFromResponse.ingredientType}`] : req.body.callType === 'add'? 1.0 : -1.0},
            $set : {'totalPrice' : finalData.newPrice, [`purchasable`]  : finalData.sum}
        },
        // options 
        {
            "multi" : false,  // update only one document 
            "upsert" : false  // insert a new document, if no existing document match the query 
        },
        function(err, object) {
            if (err){
                res.status(500).send({
                    message:
                      err.message || "Some error occurred while retrieving notes."
                  });
            }else{
                Ingredients.find()
                .then(ingredients => {
                    res.send(ingredients);
                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while retrieving notes."
                    });
                });
            }
        }
    )
}

const addIngredientHandler = (state, type) => {
    const oldCount = state.ingredients[type];
    const updatedCount = oldCount + 1;
    const updatedIngredent = {
        ...state.ingredients
    }
    updatedIngredent[type] = updatedCount;
    const oldPrice = state.totalPrice;
    const newPrice = oldPrice + state.INGREDIENT_PRICE[type];

    const updateData = updatePurchasable(state, newPrice, updatedIngredent);
    return updateData;

}

const removeIngredienthandler = (state, type) => {
    if(state.ingredients[type] > 0) {
        const oldCount = state.ingredients[type];
        const updatedCount = oldCount - 1;
        const updatedIngredent = {
            ...state.ingredients
        }
        updatedIngredent[type] = updatedCount;
        const oldPrice = state.totalPrice;
        const newPrice = oldPrice - state.INGREDIENT_PRICE[type];

        const updateData = updatePurchasable(state, newPrice, updatedIngredent);
        return updateData;
    }
}

const updatePurchasable =  (state, newPrice, updatedIngredent) => {
    const ingredents = {
        ...updatedIngredent
    }
    const sum = Object.keys(ingredents).map((igKey) => {
        return ingredents[igKey];
    }).reduce((sum,el) => {
        return sum + el;
    },0)
    return { newPrice, 'sum': sum > 0};
}
