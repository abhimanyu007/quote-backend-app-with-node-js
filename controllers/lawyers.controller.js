const lawyers = require('../modals/lawyers.modal')

// Retrieve and return all quotes from the database.
exports.findAll = (req, res) => {
    lawyers.find()
    .then(lawyers => {
        res.send(lawyers);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};