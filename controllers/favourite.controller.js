const Favourite = require("../modals/favourites.modal");

// Create and Save a new Favourites
// Create and Save a new Note
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    return res.status(400).send({
      message: "Note content can not be empty"
    });
  }

  // Create a Note
  const favourite = new Favourite({
      _id : req.body['_id'],
      person: req.body.person,
      text: req.body.text,
      isFavourite : req.body.isFavourite
  });

  // Save Note in the database
  favourite
    .save()
    .then(data => {
      Favourite.find().then(favourites => {
        console.log("quotes");
        res.send({
          response: favourites,
          message: "Quote successfully Added to Favourites",
          quotesId: data["_id"]
        });
      }).catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving notes."
        });
      });
      
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Note."
      });
    });
};

// Retrieve and return all quotes from the database.
exports.findAll = (req, res) => {
  Favourite.find()
    .then(favourites => {
      console.log("quotes");
      res.send(favourites);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving notes."
      });
    });
};

// Find a single note with a noteId
exports.findOne = (req, res) => {
  Favourite.findById(req.params.noteId)
    .then(favourite => {
      if (!favourite) {
        return res.status(404).send({
          message: "Note not found with id " + req.params.noteId
        });
      }
      res.send(quote);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "Note not found with id " + req.params.noteId
        });
      }
      return res.status(500).send({
        message: "Error retrieving note with id " + req.params.noteId
      });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body.text) {
    return res.status(400).send({
      message: "quote text can not be empty"
    });
  }

  // Find note and update it with the request body
  Favourite.findByIdAndUpdate(
    req.params.noteId,
    {
      title: req.body.title || "Untitled Note",
      content: req.body.content
    },
    { new: true }
  )
    .then(favourite => {
      if (!favourite) {
        return res.status(404).send({
          message: "Note not found with id " + req.params.noteId
        });
      }
      res.send(favourite);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "Note not found with id " + req.params.noteId
        });
      }
      return res.status(500).send({
        message: "Error updating note with id " + req.params.noteId
      });
    });
};

// Delete a quote with the specified noteId in the request
exports.delete = (req, res) => {
  Favourite.findByIdAndRemove(req.body.quoteId).then(favourite => {
      if (!favourite) {
        return res.status(404).send({
          message: "Note not found with id " + req.body.quoteId
        });
      }
      Favourite.find().then(favourites => {
          res.send({
            message: "Note deleted successfully! with id " + req.body.quoteId,
            data: favourites
          });
        }).catch(err => {
          res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving notes."
          });
        });
    }).catch(err => {
      if (err.kind === "ObjectId" || err.name === "NotFound") {
        return res.status(404).send({
          message: "Note not found with id " + req.body.quoteId
        });
      }
      return res.status(500).send({
        message: "Could not delete note with id " + req.body.quoteId
      });
    });
};
