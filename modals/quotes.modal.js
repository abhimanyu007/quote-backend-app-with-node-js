const mongoose = require('mongoose');

const quoteSchema = mongoose.Schema({
    category: String,
    quotes: [
      {
        id: String,
        person: String,
        text: String,
        isFavourite : Boolean,
        like : Number
      }
    ],
    icon: String
}, {
    timestamps: true
});

module.exports = mongoose.model('quotes', quoteSchema);