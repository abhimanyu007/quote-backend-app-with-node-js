const mongoose = require('mongoose');

const ingredientsSchema = mongoose.Schema({
    id: String,
    ingredients: {
        "salad" : Number,
        "bacon" : Number,
        "cheese" : Number,
        "meat" : Number
    },
    totalPrice: Number,
    purchasable: Boolean,
    purchasing: Boolean,
    INGREDIENT_PRICE : {
        "salad" : Number,
        "cheese" : Number,
        "bacon" : Number,
        "meat" : Number
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('ingredients', ingredientsSchema);