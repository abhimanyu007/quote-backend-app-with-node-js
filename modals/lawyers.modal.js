const mongoose = require('mongoose');

const lawyersSchema = mongoose.Schema({
    context: String,
    count: Number,
    facets: Object,
    value: Array
}, {
    timestamps: true
})

module.exports = mongoose.model('lawyers', lawyersSchema);