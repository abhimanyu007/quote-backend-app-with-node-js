const mongoose = require('mongoose');

const favouriteSchema = mongoose.Schema({
        id: String,
        person: String,
        text: String,
        isFavourite: Boolean
}, {
    timestamps: true
});

module.exports = mongoose.model('favourites', favouriteSchema);