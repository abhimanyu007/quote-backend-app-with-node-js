

module.exports = (app) => {
    const quotes = require('../controllers/quotes.controller.js');
    const favourites = require('../controllers/favourite.controller.js')
    const ingredients = require('../controllers/ingredients.controller.js')
    const lawyers = require('../controllers/lawyers.controller.js');


    //Create New Quote
    app.post('/quotes', quotes.create);
    //Retrieve all Quotes
    app.get('/quotes', quotes.findAll);
    //Retrieve a single Quote with QuoteId
    app.get('/quotes/:quoteId', quotes.findOne);
    //update Quote by QuoteId
    app.put('/updateQuotes', quotes.update);
    //Delete Quote by QuoteId
    app.delete('/quotes/:quoteId',quotes.delete);
    //Like and DisLike Quote 
    app.put('/likeDisLikeQuote', quotes.likeDisLikeQuote);

    /* ------------------------------------------------------------------- */
    //Add Quotes to favourites Quote
    app.post('/addToFavourites', favourites.create);
    //Retrieve all Favourites Quotes
    app.get('/getAllFavourites', favourites.findAll);
    //Retrieve a single Quote with QuoteId
    app.get('/favourites/:favouriteId', favourites.findOne);
    //update Quote by QuoteId
    app.put('/updateFavourites', favourites.update);
    //Delete Quote by QuoteId
    app.delete('/deleteFavourites', favourites.delete);

    /* ------------------------------------------------------------------- */
    
    //Retrieve all Favourites Quotes
    app.get('/getAllIngredients', ingredients.findAll);
    //Retrieve a single Quote with QuoteId
    // app.get('/ingrdients/:ingredientsId', ingredients.findOne);
    //update Quote by QuoteId
    app.put('/addIngredient', ingredients.update);
    //Delete Quote by QuoteId
    // app.delete('/removeIngredient', ingredients.delete);

    // Retrive all lawyers Data
    app.get('/fetchAllLawyers', lawyers.findAll);
}